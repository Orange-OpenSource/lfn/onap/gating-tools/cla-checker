FROM nexus3.onap.org:10001/onap/integration-python:9.1.0

LABEL name="cla_checker" \
      version="v0.1.dev0" \
      architecture="x86_64"

USER root

RUN apk add --no-cache openssh-client~=8.4

USER onap

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY etc/cla-checker.conf /app/etc/
COPY cla_checker  /app/cla_checker/

ENV PYTHONPATH="${PYTHONPATH}:/app"
ENV CONFIG_FILE=/app/etc/cla-checker.conf

CMD [ "python3", "cla_checker/main.py"]
