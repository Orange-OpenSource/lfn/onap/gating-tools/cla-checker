#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from configparser import ConfigParser
from json import loads, JSONDecodeError
from os import getenv
from sys import stdout


from loguru import logger
from paho.mqtt import client as mqtt

from cla_checker.gitlab_call import GitlabCall
from cla_checker.cla_list import CLAList


def start():
    logger.remove()
    logger.add(stdout, level='DEBUG')
    config = _parse_config(_get_config())

    logger.debug("python gitlab configuration")
    userdata = {}
    userdata['private_token'] = getenv('PRIVATE_TOKEN')
    userdata['gitlab_host'] = config['gitlab']['hostname']
    userdata['external_status_check_id'] = config['gitlab']['external_status_check_id']
    userdata['force_refresh_labels'] = config['gitlab']['force_refresh_labels']
    userdata['cla_lists'] = []
    userdata['qos'] = config['mqtt']['qos']
    userdata['topic'] = config['mqtt']['topic']
    for cla_list in config['gerrit']['cla_lists']:
        userdata['cla_lists'].append(CLAList(
            cla_list,
            gerrit_host=config['gerrit']['hostname'],
            gerrit_port=config['gerrit']['port'],
            refresh_interval=config['gerrit']['refresh_interval'],
        ))
    logger.debug("MQTT client creation")
    client = _create_mqtt_client(
        config,
        'mqtt.client.subscribe',
        on_connect_subscribe,
        on_message_callback=on_message,
        userdata=userdata
    )
    client.loop_forever()


def cla_check(gitlab_call, cla_lists, force_refresh=False):
    authors_list = gitlab_call.get_authors_list()
    if len(authors_list) > 0:
        authors_with_cla = []
        authors_without_cla = []
        logger.debug(
            "checking if authors are part of the {} provided lists",
            len(cla_lists),
            )
        for author in authors_list:
            author_found = False
            for cla_list in cla_lists:
                if cla_list.is_user_present(
                    author,
                    force_refresh=force_refresh,
                ):
                    author_found = True
                    break
            if author_found:
                authors_with_cla.append(author)
            else:
                authors_without_cla.append(author)
        if len(authors_without_cla) == 0:
            logger.info(
                "all {} authors have CLA approved, approving",
                len(authors_with_cla),
            )
            gitlab_call.approve_pipeline()
        else:
            logger.info(
                "the following authors doesn't have their CLA approved:"
            )
            message = "the following authors doesn't have their CLA approved:"
            for author in authors_without_cla:
                logger.info("   * {}", author)
                message += "\n   * {}".format(author)
            gitlab_call.comment(message)
    else:
        logger.warning("No authors found")
        gitlab_call.comment("No authors found, please investigate")


def on_connect_subscribe(client, userdata, _flags, return_code):
    """Use when subscribe client is connected.

    Function launched when subscribe client is connected.
    After connection, subscribe to the parent topic is done.

    :param _client: the client that has been connected
    :param _userdata: the userdata defined on the client side
    :param _flags: the flags received
    :param rc: return code of the connection

    :type _client: paho.mqtt.client.Client
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - qos: the QoS for the messenging part
    :type flags: string -- not used --
    :type return_code: integer

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.info(
        'connected to MQTT Broker with status {} for subscribing',
        str(return_code),
    )
    qos = userdata['qos']
    topic = userdata['topic']
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    logger.info(
        'subscribing on topic {} with qos {}',
        topic,
        qos,
    )
    client.subscribe('{}/#'.format(topic), qos)


def on_message(_client, userdata, msg):
    """Use when message is received.

    Function launched when a message is received on MQTT topic.

    :param _client: the client that has received the message
    :param userdata: the userdata defined on the client side
    :param msg: the message object

    :type _client: paho.mqtt.client.Client  -- not used --
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - subtopics: all the subtopics we're interested in
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - master: the Master instance which all the messages
    :type msg: paho.mqtt.client.MQTTMessage

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.debug('received a message')
    try:
        json_message = loads(msg.payload)
        logger.debug('message: {}', json_message)
        gitlab_call = GitlabCall(
            json_message['job'],
            json_message['pipeline_id'],
            json_message['source_project_id'],
            json_message['target_project_id'],
            json_message['merge_request_id'],
            json_message['last_commit_id'],
            userdata['private_token'],
            userdata['external_status_check_id'],
            host=userdata['gitlab_host'],
        )
        force_refresh = False
        label = json_message.get('label')
        if (label and label in userdata['force_refresh_labels']):
            force_refresh = True
        cla_check(
            gitlab_call,
            userdata['cla_lists'],
            force_refresh=force_refresh,
        )
    except JSONDecodeError:
        logger.error("This is not a valid JSON payload")
    except KeyError as exc:
        logger.error("the key {} wasn't found in the dict", str(exc))


def _get_config():
    """Read the config file and return the configuration.

    :returns: configparser.ConfigParser object

    :seealso: https://docs.python.org/3/library/configparser.html
    """
    config_file = getenv('CONFIG_FILE')
    logger.info('retrieving {} configuration file', config_file)
    config = ConfigParser()
    config.optionxform = lambda option: option
    config.read(config_file)
    return config


def _parse_config(config):
    force_refresh_labels_raw = config.get('gitlab', 'force_refresh_labels')
    force_refresh_labels = [
        v for v in force_refresh_labels_raw.split('\n') if v != ''
    ]
    cla_lists_raw = config.get('gerrit', 'cla_lists')
    cla_lists = [
        v for v in cla_lists_raw.split('\n') if v != ''
    ]
    # Configure auth
    auth = None
    mqtt_username = config.get('mqtt', 'username', fallback=None)
    mqtt_password = config.get('mqtt', 'password', fallback=None)
    if mqtt_username:
        auth = {'username': mqtt_username}
        if mqtt_password:
            auth['password'] = mqtt_password

    config = {
        'gerrit': {
            'cla_lists': cla_lists,
            'hostname': config.get('gerrit', 'hostname'),
            'port': config.getint('gerrit', 'port', fallback=29418),
            'refresh_interval': config.getint(
                'gerrit',
                'refresh_interval',
                fallback=86400,
                ),
        },
        'gitlab': {
            'external_status_check_id': config.getint(
                'gitlab',
                'external_status_check_id',
                ),
            'hostname': config.get('gitlab', 'hostname'),
            'force_refresh_labels': force_refresh_labels,
        },
        'mqtt': {
            'qos': config.getint('mqtt', 'qos', fallback=0),
            'port': config.getint('mqtt', 'port', fallback=1883),
            'keepalive': config.getint('mqtt', 'keepalive', fallback=60),
            'hostname': config.get('mqtt', 'hostname'),
            'auth': auth,
            'topic': config.get('mqtt', 'topic'),
            'transport': config.get('mqtt', 'transport', fallback='tcp'),
        },
    }
    logger.debug("config parsed: {}", config)
    return config


def _create_mqtt_client(
    config,
    name,
    on_connect_callback,
    on_message_callback=None,
    userdata=None,
):
    """Configure, create and connect an MQTT client.

    :param config: the config to apply
    :param name: the name of the client
    :param on_connect_callback: the on_connect callback method
    :param on_message_callback: the on_message callback method
    :param userdata: the userdata to add to the client

    :type config:  dict
    :type name: string
    :type on_connect_callback: function name
    :type userdata: dict, default to None
    :type on_message_callback: function name, default to None

    :returns: an MQTT client

    :rtype: paho.mqtt.client.Client
    """
    logger.info('configure and connect MQTT client {}', name)
    mqtt_config = config['mqtt']
    client = mqtt.Client(
        transport=mqtt_config['transport'],
        userdata=userdata,
    )
    client.on_connect = on_connect_callback
    if on_message_callback:
        client.on_message = on_message_callback
    if mqtt_config['transport'] == 'websockets':
        client.ws_set_options(
            path=mqtt_config['mqtt_path'],
            headers=mqtt_config['mqtt_headers'],
        )
    client.enable_logger(logger)
    client.connect(
        mqtt_config['hostname'],
        mqtt_config['port'],
        mqtt_config['keepalive'],
    )
    return client


if __name__ == '__main__':
    start()
