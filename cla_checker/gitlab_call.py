# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
from dataclasses import dataclass
import re
from time import sleep
from typing import List

import gitlab
from gitlab.v4.objects import ProjectJob, ProjectPipeline, ProjectPipelineJob
from loguru import logger
import requests


@dataclass
class GitlabCall:  # pylint: disable=too-many-instance-attributes
    # For now, we'll keep all these attributes
    # needs to be refactored one day
    job: str
    pipeline_id: int
    source_project_id: int
    target_project_id: int
    merge_request_id: int
    last_commit_id: str
    private_token: str
    external_status_check_id: int
    host: str = "https://gitlab.com"
    wait_time: int = 10
    max_retries: int = 90

    def __post_init__(self) -> None:
        self.gitlab = gitlab.Gitlab(self.host, private_token=self.private_token)
        self.source_project = self.gitlab.projects.get(
            self.source_project_id,
            retry_transient_errors=True,
        )
        self.target_project = self.gitlab.projects.get(
            self.target_project_id,
            retry_transient_errors=True,
        )
        self.merge_request = self.target_project.mergerequests.get(
            self.merge_request_id,
            retry_transient_errors=True,
        )

    def get_pipeline(self) -> ProjectPipeline:

        pipeline = None
        try:
            logger.debug(
                "Searching pipeline on source project {} ({})",
                self.source_project.name,
                self.source_project_id,
                )
            pipeline = self.source_project.pipelines.get(
                self.pipeline_id,
                retry_transient_errors=True,
            )
            logger.debug(
                "pipeline {} has been found on source project {} ({})",
                self.pipeline_id,
                self.source_project.name,
                self.source_project_id,
                )
        except gitlab.exceptions.GitlabGetError:
            logger.warning("pipeline not found in source project")
        if not pipeline:
            logger.debug("Searching pipeline on target project {} ({})",
                self.target_project.name,
                self.target_project_id,
                )
            try:
                pipeline = self.target_project.pipelines.get(
                    self.pipeline_id,
                    retry_transient_errors=True,
                )
                logger.debug(
                    "pipeline {} has been found on target project {} ({})",
                    self.pipeline_id,
                    self.target_project.name,
                    self.target_project_id,
                    )
            except gitlab.exceptions.GitlabGetError:
                logger.error("pipeline not found also in target project")
        return pipeline

    def get_job(self) -> ProjectJob:
        pipeline = self.get_pipeline()
        if pipeline:
            jobs = pipeline.jobs.list(retry_transient_errors=True)
            logger.debug(
                "{} jobs are present in the pipeline",
                len(jobs),
            )
            for job in jobs:
                logger.debug("checking job {}", job.name)
                if job.name == self.job:
                    return job
        logger.warning("job {} not found", self.job)
        return None

    def wait_for_end(self) -> ProjectJob:
        retry = 0
        job_id = "Unknown"
        while retry < self.max_retries:
            job = self.get_job()
            if job:
                job_id = job.id
                logger.debug(
                    "check if job {} has finished, current_status: {}",
                    job.id,
                    job.status,
                )
                if job.status == "success":
                    logger.debug("job {} finished successfully", job.id)
                    return self.get_project_job(job)
                if job.status == "failed":
                    logger.warning("job {} has failed", job.id)
                    return None
                if job.status == "canceled":
                    logger.debug("job {} has been canceled", job.id)
                    return None
                if job.status == "skipped":
                    logger.debug("job {} has been skipped", job.id)
                    return None
            sleep(self.wait_time)
            retry += 1
        logger.debug("job {} was too long to finish", job_id)
        return None

    def get_project_job(self, job: ProjectPipelineJob) -> ProjectJob:
        project_job = None
        if job.project_id == self.source_project_id:
            project_job = self.source_project.jobs.get(
                job.id,
                retry_transient_errors=True,
                )
        if job.project_id == self.target_project_id:
            project_job = self.target_project.jobs.get(
                job.id,
                retry_transient_errors=True,
                )
        return project_job

    def get_authors_list(self) -> List[str]:
        job = self.wait_for_end()
        authors_list = []
        if job:
            logger.debug("retrieving trace for job {}({})", job.name, job.id)
            trace = job.trace().decode("utf-8")
            pattern = re.compile(
                r'<------------- AUTHORS LIST ------------->[\s\S]*'
                + r'<---------------------------------------->',
                re.MULTILINE,
            )
            if pattern.search(trace):
                user_list_raw = pattern.search(trace).group(0)
                for line in user_list_raw.split('\n'):
                    email = line.split('\t')[-1]
                    if '@' in email:
                        authors_list.append(email)
        return authors_list

    def comment(self, message: str) -> None:
        self.merge_request.notes.create({'body': message})

    def approve_pipeline(self) -> None:
        headers = {'PRIVATE-TOKEN': self.private_token}
        params = {
            'sha': self.last_commit_id,
            'external_status_check_id': self.external_status_check_id,
        }
        base_url = "{}/api/v4".format(self.host)
        project_url = "{}/projects/{}".format(base_url, self.target_project_id)
        url = "{}/merge_requests/{}/status_check_responses".format(
            project_url,
            self.merge_request_id,
        )
        logger.debug("url to send: {}", url)
        logger.debug("params used: {}", params)
        request = requests.post(url, params=params, headers=headers)
        logger.debug("gitlab return: {}", request.text)
