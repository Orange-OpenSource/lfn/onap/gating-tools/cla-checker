# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
from dataclasses import dataclass, field
import subprocess  #nosec
import time
from typing import List


from loguru import logger


@dataclass
class CLAList:
    group: str
    users: List[str] = field(default_factory=list)
    gerrit_host: str = "gerrit.onap.org"
    gerrit_port: int = 29418
    last_refresh: float = 0.0
    refresh_interval: int = 86400

    def refresh_list(self):
        logger.debug("nb users before refresh: {}", len(self.users))
        arguments = [
            "ssh",
            "-p",
            str(self.gerrit_port),
            self.gerrit_host,
            "gerrit",
            "ls-members",
            self.group,
        ]
        gerrit_list = ""
        try:
            # This is the only way to retrieve user list for now.
            # Output is only used for email list and input comes from
            # configuration so security hole is thin.
            gerrit_list_result = subprocess.run(  # nosec
                arguments, capture_output=True, check=True, text=True)
            gerrit_list = gerrit_list_result.stdout
            for line in gerrit_list.split('\n'):
                email = line.split('\t')[-1]
                if '@' in email:
                    if not email in self.users:
                        self.users.append(email)
            logger.debug("successful refresh done")
            self.last_refresh = time.time()
        except subprocess.TimeoutExpired:
            logger.error("unable to complete the refresh command")
        except subprocess.CalledProcessError as exc:
            logger.error("error when trying to refresh the list")
            logger.error(exc.stdout)
            logger.error(exc.stderr)
        logger.debug("nb users after refresh: {}", len(self.users))

    def is_user_present(self, user, force_refresh=False):
        logger.debug(
            "checking if user {} is present in the CLA list {}",
            user,
            self.group,
            )
        current_time = time.time()
        need_refresh = current_time > (
            self.last_refresh + self.refresh_interval)
        if force_refresh:
            logger.debug("force refresh asked")
        if need_refresh:
            logger.debug("list is not fresh enough, refreshing")
        if force_refresh or need_refresh:
            self.refresh_list()
        return user in self.users
