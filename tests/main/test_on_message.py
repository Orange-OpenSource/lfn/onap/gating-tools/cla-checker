# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

from paho.mqtt.client import MQTTMessage
import mock

from cla_checker import main

@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_OK_no_force_refresh_1(  # pylint: disable=invalid-name
    gitlab_call_mock,
    cla_check_mock,
    valid_msg,
    valid_userdata,
):
    main.on_message(None, valid_userdata, valid_msg)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        1234,
        5678,
        9012,
        3456,
        'abc',
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    cla_check_mock.assert_called_once_with(
        mock.ANY,
        ['list_1', 'list_2'],
        force_refresh=False,
    )


@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_OK_no_force_refresh_2(  # pylint: disable=invalid-name
    gitlab_call_mock,
    cla_check_mock,
    valid_msg,
    valid_userdata,
):
    msg_raw = json.loads(valid_msg.payload)
    msg_raw.pop('label')
    msg = MQTTMessage()
    msg.payload = json.dumps(msg_raw)
    main.on_message(None, valid_userdata, msg)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        1234,
        5678,
        9012,
        3456,
        'abc',
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    cla_check_mock.assert_called_once_with(
        mock.ANY,
        ['list_1', 'list_2'],
        force_refresh=False,
    )

@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_OK_force_refresh(  # pylint: disable=invalid-name
    gitlab_call_mock,
    cla_check_mock,
    valid_msg,
    valid_userdata,
):
    user_data = valid_userdata
    user_data['force_refresh_labels'].append('label_1')
    main.on_message(None, user_data, valid_msg)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        1234,
        5678,
        9012,
        3456,
        'abc',
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    cla_check_mock.assert_called_once_with(
        mock.ANY,
        ['list_1', 'list_2'],
        force_refresh=True,
    )


@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_no_msg_JSON(   # pylint: disable=invalid-name
    gitlab_call_mock,
    cla_check_mock,
    valid_userdata,
    caplog,
):
    msg = MQTTMessage()
    msg.payload = 'test'
    main.on_message(None, valid_userdata, msg)
    gitlab_call_mock.assert_not_called()
    cla_check_mock.assert_not_called()
    assert "This is not a valid JSON payload" in caplog.text

@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_bad_msg(
    gitlab_call_mock,
    cla_check_mock,
    valid_msg,
    valid_userdata,
    caplog
):
    msg_raw = json.loads(valid_msg.payload)
    msg_raw.pop('source_project_id')
    msg = MQTTMessage()
    msg.payload = json.dumps(msg_raw)
    main.on_message(None, valid_userdata, msg)
    gitlab_call_mock.assert_not_called()
    cla_check_mock.assert_not_called()
    assert  "the key 'source_project_id' wasn't found in the dict" in caplog.text

@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_bad_userdata_1(
    gitlab_call_mock,
    cla_check_mock,
    valid_msg,
    valid_userdata,
    caplog
):
    userdata = valid_userdata
    userdata.pop('external_status_check_id')
    main.on_message(None, userdata, valid_msg)
    gitlab_call_mock.assert_not_called()
    cla_check_mock.assert_not_called()
    assert "the key 'external_status_check_id' wasn't found in the dict" in caplog.text

@mock.patch('cla_checker.main.cla_check')
@mock.patch('cla_checker.main.GitlabCall')
def test_bad_userdata_2(
    gitlab_call_mock,
    cla_check_mock,
    valid_msg,
    valid_userdata,
    caplog
):
    userdata = valid_userdata
    userdata.pop('cla_lists')
    main.on_message(None, userdata, valid_msg)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        1234,
        5678,
        9012,
        3456,
        'abc',
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    cla_check_mock.assert_not_called()
    assert "the key 'cla_lists' wasn't found in the dict" in caplog.text
