# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

from  cla_checker import main

def is_user_present_1(author, force_refresh=False):  # pylint: disable=unused-argument
    users = ['test@test.fr', 'test4@test.fr']
    return author in users

def is_user_present_2(author, force_refresh=False):  # pylint: disable=unused-argument
    users = ['test2@test.fr', 'test5@test.fr']
    return author in users

def test_no_authors_found(caplog):
    gitlab_call = mock.MagicMock()
    gitlab_call.get_authors_list.return_value = []
    gitlab_call.comment.return_value = "OK"
    cla_list_1 = mock.MagicMock()
    cla_lists = [cla_list_1]
    main.cla_check(gitlab_call, cla_lists)
    cla_list_1.assert_not_called()
    assert "No authors found" in caplog.text
    gitlab_call.comment.assert_called_once_with("No authors found, please investigate")

def test_some_authors_without_cla(caplog):
    gitlab_call = mock.MagicMock()
    gitlab_call.get_authors_list.return_value = [
        'test@test.fr',
        'test2@test.fr',
        'test3@test.fr',
        ]
    gitlab_call.comment.return_value = "OK"
    cla_list_1 = mock.MagicMock()
    cla_list_1.is_user_present.side_effect = is_user_present_1
    cla_list_2 = mock.MagicMock()
    cla_list_2.is_user_present.side_effect = is_user_present_2
    cla_lists = [cla_list_1, cla_list_2]
    main.cla_check(gitlab_call, cla_lists)
    assert "the following authors doesn't have their CLA approved:" in caplog.text
    assert "   * test3@test.fr" in caplog.text
    msg = "the following authors doesn't have their CLA approved:"
    msg += "\n   * test3@test.fr"
    gitlab_call.comment.assert_called_once_with(msg)

def test_all_authors_with_cla(caplog):
    gitlab_call = mock.MagicMock()
    gitlab_call.get_authors_list.return_value = [
        'test@test.fr',
        'test2@test.fr',
        'test5@test.fr',
        ]
    gitlab_call.approve_pipeline.return_value = "OK"
    cla_list_1 = mock.MagicMock()
    cla_list_1.is_user_present.side_effect = is_user_present_1
    cla_list_2 = mock.MagicMock()
    cla_list_1.is_user_present.side_effect = is_user_present_2
    cla_lists = [cla_list_1, cla_list_2]
    main.cla_check(gitlab_call, cla_lists)
    assert "all 3 authors have CLA approved, approving" in caplog.text
    gitlab_call.approve_pipeline.assert_called_once()
