# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os

import mock

from  cla_checker import main

@mock.patch.dict(os.environ, {"CONFIG_FILE": "etc/cla-checker.conf", 'PRIVATE_TOKEN': 'token'})
@mock.patch('cla_checker.main._create_mqtt_client')
@mock.patch('cla_checker.main.CLAList')
def test_start(clalist_mock, create_mqtt_client_mock, valid_userdata, valid_config_parsed):
    mqtt_client_mock = mock.MagicMock()
    create_mqtt_client_mock.return_value = mqtt_client_mock
    expected_userdata = valid_userdata
    expected_userdata['cla_lists'] = [mock.ANY, mock.ANY]
    main.start()
    calls = [
        mock.call(
            'list_1',
            gerrit_host='gerrit.example.com',
            gerrit_port=29418, refresh_interval=86400,
            ),
        mock.call(
            'list_2',
            gerrit_host='gerrit.example.com',
            gerrit_port=29418, refresh_interval=86400,
            ),
        ]
    clalist_mock.assert_has_calls(calls)
    create_mqtt_client_mock.assert_called_once_with(
        valid_config_parsed,
        'mqtt.client.subscribe',
        main.on_connect_subscribe,
        on_message_callback=main.on_message,
        userdata=expected_userdata,
    )
    mqtt_client_mock.loop_forever.assert_called_once()
