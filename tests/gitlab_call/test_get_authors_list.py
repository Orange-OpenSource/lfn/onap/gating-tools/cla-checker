# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

from conftest import GitlabMock

from cla_checker.gitlab_call import GitlabCall


@mock.patch('gitlab.Gitlab')
@mock.patch.object(GitlabCall, 'wait_for_end')
def test_get_authors_list_no_job(mock_wait_for_end, mock_gitlab):
    mock_gitlab.return_value = GitlabMock()
    mock_wait_for_end.return_value = None
    gitlab_call = GitlabCall("job", 1234, 5678, 9012, 3456, "abc", "token", 12)
    authors = gitlab_call.get_authors_list()
    assert len(authors) == 0


@mock.patch('gitlab.Gitlab')
@mock.patch.object(GitlabCall, 'wait_for_end')
def test_get_authors_list_job_bad_trace(mock_wait_for_end, mock_gitlab):
    mock_gitlab.return_value = GitlabMock()
    mock_job = mock.Mock()
    mock_wait_for_end.return_value = mock_job
    mock_job.trace.return_value = "trace".encode("utf-8")
    gitlab_call = GitlabCall("job", 1234, 5678, 9012, 3456, "abc", "token", 12)
    authors = gitlab_call.get_authors_list()
    assert len(authors) == 0


@mock.patch('gitlab.Gitlab')
@mock.patch.object(GitlabCall, 'wait_for_end')
def test_get_authors_list_job_good_trace_1(mock_wait_for_end, mock_gitlab):
    mock_gitlab.return_value = GitlabMock()
    mock_job = mock.Mock()
    mock_wait_for_end.return_value = mock_job
    mock_job.trace.return_value = """
test1@test.fr
<------------- AUTHORS LIST ------------->
test@test.fr
1@2.fr
<---------------------------------------->
""".encode("utf-8")
    gitlab_call = GitlabCall("job", 1234, 5678, 9012, 3456, "abc", "token", 12)
    authors = gitlab_call.get_authors_list()
    assert len(authors) == 2
    assert 'test@test.fr' in authors
    assert '1@2.fr' in authors
