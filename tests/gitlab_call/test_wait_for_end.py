# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

from cla_checker.gitlab_call import GitlabCall

@mock.patch.object(GitlabCall, 'get_project_job')
@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_success(mock_get_job, mock_get_project_job, gitlab_call):
    mock_job = mock.MagicMock(id=12, status='success')
    mock_project_job = mock.MagicMock()
    mock_get_job.return_value = mock_job
    mock_get_project_job.return_value = mock_project_job
    assert gitlab_call.wait_for_end() == mock_project_job
    mock_get_job.assert_called_once()
    mock_get_project_job.assert_called_once_with(mock_job)

@mock.patch.object(GitlabCall, 'get_project_job')
@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_running_success(mock_get_job, mock_get_project_job, gitlab_call):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "running",
            "running",
            "success",
            "success",
            "skipped",
            "skipped",
            "skipped",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_project_job = mock.MagicMock()
    mock_get_job.return_value = mock_job
    mock_get_project_job.return_value = mock_project_job
    assert gitlab_call.wait_for_end() == mock_project_job
    assert mock_get_job.call_count == 2
    mock_get_project_job.assert_called_once_with(mock_job)


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_failed(mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status='failed')
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 has failed" in caplog.text
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_running_failed(mock_get_job, gitlab_call, caplog):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "running",
            "running",
            "failed",
            "failed",
            "failed",
            "skipped",
            "skipped",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 has failed" in caplog.text
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_canceled(mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status='canceled')
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 has been canceled" in caplog.text
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_running_canceled(mock_get_job, gitlab_call, caplog):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "running",
            "running",
            "canceled",
            "canceled",
            "canceled",
            "canceled",
            "skipped",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 has been canceled" in caplog.text
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_skipped(mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status='skipped')
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 has been skipped" in caplog.text
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_running_skipped(mock_get_job, gitlab_call, caplog):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "running",
            "running",
            "skipped",
            "skipped",
            "skipped",
            "skipped",
            "skipped",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 has been skipped" in caplog.text
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_timeout(mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status='running')
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() is None
    assert "job 12 was too long to finish" in caplog.text
    assert mock_get_job.call_count == 5


@mock.patch.object(GitlabCall, 'get_job')
def test_wait_for_end_no_job(mock_get_job, gitlab_call, caplog):
    mock_get_job.return_value = None
    assert gitlab_call.wait_for_end() is None
    assert "job Unknown was too long to finish" in caplog.text
    assert mock_get_job.call_count == 5
