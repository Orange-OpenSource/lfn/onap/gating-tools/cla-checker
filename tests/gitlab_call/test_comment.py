# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

from conftest import GitlabMock

from cla_checker.gitlab_call import GitlabCall


@mock.patch('gitlab.Gitlab')
def test_comment(mock_gitlab):
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall("job", 1234, 5678, 9012, 3456, "abc", "token", 12)
    gitlab_call.comment('comment')
    expected_calls = [mock.call.create({'body': 'comment'})]
    assert gitlab_call.merge_request.notes.method_calls == expected_calls
