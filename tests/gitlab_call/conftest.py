# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

from gitlab.exceptions import GitlabGetError
import pytest

from cla_checker.gitlab_call import GitlabCall

class JobMock():  # pylint: disable=too-few-public-methods
    def __init__(self, name, id, project_id: int = 0) -> None:  # pylint: disable=redefined-builtin,invalid-name
        self.project_id = project_id
        self.id = id  # pylint: disable=redefined-builtin,invalid-name
        self.name = name

class JobsMock:  # pylint: disable=too-few-public-methods
    def __init__(self, name, id) -> None:  # pylint: disable=redefined-builtin,invalid-name
        self.job = JobMock(name, id)

    def get(self, _id, retry_transient_errors=True):  # pylint: disable=unused-argument
        return self.job
class MergeRequestMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.notes = mock.Mock()


class MergeRequestsMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.mergerequest = MergeRequestMock()

    def get(self, _id, retry_transient_errors=True):  # pylint: disable=unused-argument
        return self.mergerequest


class PipelineMock():  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        pass


class PipelinesMock():  # pylint: disable=too-few-public-methods
    def __init__(self, fail_on_33=False) -> None:
        self.pipeline = PipelineMock()
        self.fail_on_33 = fail_on_33

    def get(self, id, retry_transient_errors=True):  # pylint: disable=redefined-builtin,invalid-name,unused-argument
        if self.fail_on_33 and id == 33:
            raise GitlabGetError
        if id == 75:
            raise GitlabGetError
        return self.pipeline


class ProjectMock:  # pylint: disable=too-few-public-methods
    def __init__(self, source=False) -> None:
        self.mergerequests = MergeRequestsMock()
        self.jobs = JobsMock('target job', 1)
        self.name = "target project"
        self.pipelines = PipelinesMock()
        if source:
            self.pipelines = PipelinesMock(fail_on_33=True)
            self.name = "source project"
            self.jobs = JobsMock('source job', 2)


class ProjectsMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.target_project = ProjectMock()
        self.source_project = ProjectMock(source=True)

    def get(self, id, retry_transient_errors=True): # pylint: disable=redefined-builtin,invalid-name,unused-argument
        if id in [64, 5678]:
            return self.source_project
        return self.target_project


class GitlabMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.projects = ProjectsMock()

@pytest.fixture
def gitlab_call():
    with mock.patch('gitlab.Gitlab') as mock_gitlab:
        mock_gitlab.return_value = GitlabMock()
        return GitlabCall(
            "two",
            1234,
            5678,
            9012,
            3456,
            "abc",
            "token",
            12,
            wait_time=0,
            max_retries=5,
            )
