# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
from conftest import JobMock

def test_job_in_source(gitlab_call):
    job = JobMock('job', 0, project_id=5678)
    project_job = gitlab_call.get_project_job(job)
    assert project_job.name == "source job"

def test_job_in_target(gitlab_call):
    job = JobMock('job', 0, project_id=9012)
    project_job = gitlab_call.get_project_job(job)
    assert project_job.name == "target job"

def test_job_not_in_source_or_target(gitlab_call):
    job = JobMock('job', 0, project_id=0)
    project_job = gitlab_call.get_project_job(job)
    assert project_job is None
