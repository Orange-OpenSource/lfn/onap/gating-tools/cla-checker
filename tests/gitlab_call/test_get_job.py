# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
from typing import List

import mock

from conftest import GitlabMock

from cla_checker.gitlab_call import GitlabCall


class MockJob:  # pylint: disable=too-few-public-methods
    def __init__(self, name: str) -> None:
        self.name = name


class MockJobs: # pylint: disable=too-few-public-methods
    def __init__(self, jobs: List[MockJob]) -> None:
        self.jobs = jobs

    def list(self, retry_transient_errors=False) -> List[MockJob]: # pylint: disable=unused-argument
        return self.jobs


@mock.patch.object(GitlabCall, 'get_pipeline')
def test_get_job_success(mock_get_pipeline, gitlab_call):
    job_1 = MockJob("one")
    job_2 = MockJob("two")
    job_3 = MockJob("three")
    mock_jobs = MockJobs([job_1, job_2, job_3])
    mock_pipeline = mock.MagicMock(id=12, jobs=mock_jobs)
    mock_get_pipeline.return_value = mock_pipeline
    assert gitlab_call.get_job() == job_2


@mock.patch('gitlab.Gitlab')
@mock.patch.object(GitlabCall, 'get_pipeline')
def test_get_job_not_found(mock_get_pipeline, mock_gitlab, caplog):
    mock_gitlab.return_value = GitlabMock()
    job_1 = MockJob("one")
    job_2 = MockJob("two")
    job_3 = MockJob("three")
    mock_jobs = MockJobs([job_1, job_2, job_3])
    mock_pipeline = mock.MagicMock(id=12, jobs=mock_jobs)
    mock_get_pipeline.return_value = mock_pipeline
    gitlab_call = GitlabCall("four", 1234, 5678, 9012, 3456, "abc", "token", 12)
    assert gitlab_call.get_job() is None
    assert "job four not found" in caplog.text


@mock.patch('gitlab.Gitlab')
@mock.patch.object(GitlabCall, 'get_pipeline')
def test_get_job_no_pipeline(mock_get_pipeline, mock_gitlab, caplog):
    mock_gitlab.return_value = GitlabMock()
    mock_get_pipeline.return_value = None
    gitlab_call = GitlabCall("four", 1234, 5678, 9012, 3456, "abc", "token", 12)
    assert gitlab_call.get_job() is None
    assert "job four not found" in caplog.text
