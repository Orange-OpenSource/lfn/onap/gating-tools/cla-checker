# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

class RequestMock():  # pylint: disable=too-few-public-methods
    def __init__(self, text: str) -> None:
        self.text = text

@mock.patch("requests.post")
def test_approve_pipeline(mock_requests, gitlab_call):
    mock_requests.return_value = RequestMock("text")
    gitlab_call.approve_pipeline()
    base_url = "https://gitlab.com/api/v4/projects/9012"
    expected_url = "{}/merge_requests/3456/status_check_responses".format(base_url)
    expected_headers = {'PRIVATE-TOKEN': 'token'}
    expected_params = {
        'sha': 'abc',
        'external_status_check_id': 12,
    }
    mock_requests.assert_called_once_with(
        expected_url,
        params=expected_params,
        headers=expected_headers,
    )
