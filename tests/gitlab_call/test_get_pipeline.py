# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

from conftest import GitlabMock, PipelineMock

from cla_checker.gitlab_call import GitlabCall

def test_source_found(gitlab_call, caplog):
    assert isinstance(gitlab_call.get_pipeline(), PipelineMock)
    assert "pipeline source 1234 has been found on source project" not in caplog.text
    assert "pipeline not found in source project" not in caplog.text
    assert "Searching pipeline on target project" not in caplog.text
    assert "pipeline not found also in target project" not in caplog.text

@mock.patch('gitlab.Gitlab')
def test_target_found(mock_gitlab, caplog):
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall("two", 33, 64, 9012, 3456, "abc", "token", 12)
    assert isinstance(gitlab_call.get_pipeline(), PipelineMock)
    assert "pipeline source 1234 has been found on target project" not in caplog.text
    assert "pipeline not found in source project" in caplog.text
    assert "Searching pipeline on target project" in caplog.text
    assert "pipeline not found also in target project" not in caplog.text


@mock.patch('gitlab.Gitlab')
def test_target_not_found(mock_gitlab, caplog):
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall("two", 75, 64, 9012, 3456, "abc", "token", 12)
    assert gitlab_call.get_pipeline() is None
    assert "pipeline not found in source project" in caplog.text
    assert "Searching pipeline on target project" in caplog.text
    assert "pipeline not found also in target project" in caplog.text
