# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import subprocess  # nosec
import time

import mock

from cla_checker.cla_list import CLAList

class MockRun(): # pylint: disable=too-few-public-methods
    def __init__(self, stdout, cmd="", returncode=0):
        self.stdout = stdout
        self.cmd = cmd
        self.returncode = returncode

@mock.patch("subprocess.run")
def test_cla_list_refresh_ok(run_mock):
    cla_list = CLAList(
        "groupe",
        gerrit_host="gerrite",
        gerrit_port=1234,
        )
    stdout = """
test@test.fr
1@2.fr
3

    """
    launch_time = time.time()
    run_mock.return_value = MockRun(stdout)
    cla_list.refresh_list()
    arguments = [
        "ssh",
        "-p",
        "1234",
        "gerrite",
        "gerrit",
        "ls-members",
        "groupe",
    ]
    run_mock.assert_called_once_with(
        arguments,
        capture_output=True,
        check=True,
        text=True,
        )
    assert cla_list.last_refresh > launch_time
    assert len(cla_list.users) == 2
    assert "test@test.fr" in cla_list.users
    assert "1@2.fr" in cla_list.users

@mock.patch("subprocess.run", side_effect=subprocess.TimeoutExpired("ssh", 5.0, "nope"))
def test_cla_list_refresh_timeout(_run_mock, caplog):
    cla_list = CLAList(
        "groupe",
        gerrit_host="gerrite",
        gerrit_port=1234,
        )
    cla_list.refresh_list()
    assert len(cla_list.users) == 0
    assert cla_list.last_refresh == 0.0
    assert "unable to complete the refresh command" in caplog.text

@mock.patch("subprocess.run", side_effect=subprocess.CalledProcessError(-1, "ssh"))
def test_cla_list_refresh_called_process_error(_run_mock, caplog):
    cla_list = CLAList(
        "groupe",
        gerrit_host="gerrite",
        gerrit_port=1234,
        )
    cla_list.refresh_list()
    assert len(cla_list.users) == 0
    assert cla_list.last_refresh == 0.0
    assert "error when trying to refresh the list" in caplog.text

@mock.patch.object(CLAList, 'refresh_list')
def test_is_user_present_no_refresh_not_found(mock_refresh):
    cla_list = CLAList(
        "groupe",
        last_refresh=time.time(),
        users = [
            'test1@test.fr',
            'test@test.frs',
            'ttest@test.fr'
        ]
        )
    assert cla_list.is_user_present("test@test.fr") is False
    mock_refresh.assert_not_called()

@mock.patch.object(CLAList, 'refresh_list')
def test_is_user_present_no_refresh_found(mock_refresh):
    cla_list = CLAList(
        "groupe",
        last_refresh=time.time(),
        users = [
            'test1@test.fr',
            'test@test.frs',
            'test@test.fr'
        ]
        )
    assert cla_list.is_user_present("test@test.fr") is True
    mock_refresh.assert_not_called()

@mock.patch.object(CLAList, 'refresh_list')
def test_is_user_present_refresh_found(mock_refresh):
    cla_list = CLAList(
        "groupe",
        users = [
            'test1@test.fr',
            'test@test.frs',
            'test@test.fr'
        ]
        )
    assert cla_list.is_user_present("test@test.fr") is True
    mock_refresh.assert_called_once()

@mock.patch.object(CLAList, 'refresh_list')
def test_is_user_present_force_refresh_found(mock_refresh):
    cla_list = CLAList(
        "groupe",
        last_refresh=time.time(),
        users = [
            'test1@test.fr',
            'test@test.frs',
            'test@test.fr'
        ]
        )
    assert cla_list.is_user_present("test@test.fr", force_refresh=True) is True
    mock_refresh.assert_called_once()
